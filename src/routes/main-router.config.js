import jobIcon from '../assets/job.svg';
import parcoursIcon from '../assets/parcours.svg';
import conseilIcon from '../assets/conseil.svg';
import moiIcon from '../assets/moi.svg';
// import JobsScreen from "../screen/sub/JobsScreen";
// import ConseilScreen from "../screen/sub/ConseilScreen";
import ParcoursScreen from "../screen/ParcoursScreen";

const routerConfig = [
    {
        component: <ParcoursScreen/>,
        icon: jobIcon,
        name: 'jobs',
        path: '/jobs',
    },
    {
        component: <ParcoursScreen/>,
        icon: conseilIcon,
        name: 'conseil',
        path: '/conseils',
    },
    {
        component: <ParcoursScreen/>,
        icon: parcoursIcon,
        name: 'parcours',
        path: '/mon-parcours',
    },
    {
        component: <ParcoursScreen/>,
        icon: moiIcon,
        name: 'profil',
        path: '/profil',
    },
];

export default routerConfig;
