import {
    Routes,
    Route
} from "react-router-dom";
import HomeScreen from "../screen/HomeScreen";
import ParcoursScreen from "../screen/ParcoursScreen";

function MainRouter({config}) {
    return (
        <Routes>
            <Route path="/" element={HomeScreen}/>
            {
                config.map(
                    (route, index) => (
                        <Route path={route.path} index={index === 0} key={index} element={route.component}/>
                    )
                )
            }
            <Route path="*" element={<ParcoursScreen />}/>
        </Routes>
    );
}

export default MainRouter;
