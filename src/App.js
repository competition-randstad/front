import {useState} from "react";
import MainScreen from './screen/MainScreen';
import './reset.css';
import {Route, BrowserRouter as Router, Routes} from "react-router-dom";
import WelcomeScreen from "./screen/WelcomeScreen";
import ConnexionScreen from "./screen/ConnexionScreen";
import InscriptionScreen from "./screen/InscriptionScreen";

function App() {

    const [user, setUser] = useState(null);

    if (user !== null) {
        return (
            <Router>
                <MainScreen user={user}/>
            </Router>
        );
    }

    return (
        <Router>
            <Routes>
                <Route path="/connexion" element={<ConnexionScreen setUser={setUser} />}/>
                <Route path="/inscription" element={<InscriptionScreen setUser={setUser} />}/>
                <Route path="*" element={<WelcomeScreen />}/>
            </Routes>
        </Router>
    );
}

export default App;
