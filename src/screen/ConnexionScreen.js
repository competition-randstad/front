import {useEffect} from "react";

function ConnexionScreen({setUser}) {

    useEffect(() => {setUser({})});

    return (
        <p>Connexion</p>
    );
}

export default ConnexionScreen;
