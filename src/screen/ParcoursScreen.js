import appLogo from "../assets/logo-neo-yellow.svg";
import step1 from "../assets/step-1.svg";
import step1ToDo from "../assets/step-1-to-do.svg";
import step2 from "../assets/step-2.svg";
import step2ToDo from "../assets/step-2-to-do.svg";
import step3 from "../assets/step-3.svg";
import step3ToDo from "../assets/step-3-to-do.svg";
import step4 from "../assets/step-4.svg";
import step4ToDo from "../assets/step-4-to-do.svg";
import "./parcours.css";
import {useState} from "react";

function ParcoursScreen() {

    const [step, setStep] = useState(0);

    const handleClick = (condition) => () => {
        if (step < condition) {
            setStep(step + 1);
        } else {
            setStep(step - 1);
        }
    }

    return (
        <div className="app-parcours">
            <header className="app-parcours__header">
                <img className="app-parcours__header__logo" src={appLogo} alt="Logo Neo By Randstad"/>
            </header>
            <div className="app-parcours__body">
                <div className="progress__container">
                    <svg className="progress" xmlns="http://www.w3.org/2000/svg">
                        <path d="M 20,0 v 90 c 0,30,25,60,60,80" stroke="url(#part_complete)" strokeWidth="50"
                              fill="none"/>
                        <path d="M 80,170 c 23,16,53,25,86,25 c 45,0,135,47,135,106" stroke={step >= 1 ? "url(#part_complete)" : "url(#part_to_do)"}
                              strokeWidth="50" fill="none"/>
                        <path d="M 301,301 v21c0,58.83,-59.92,106.53,-133.84,106.53" stroke={step >= 2 ? "url(#part_complete)" : "url(#part_to_do)"}
                              strokeWidth="50" fill="none"/>
                        <path d="M 167.16,428.53 c -73.91,0,-128.21,44.36,-128.21,103.2" stroke={step >= 3 ? "url(#part_complete)" : "url(#part_to_do)"}
                              strokeWidth="50" fill="none"/>
                        <defs>
                            <linearGradient id="part_complete" x1="50" y1="0" x2="367" y2="531"
                                            gradientUnits="userSpaceOnUse">
                                <stop stopColor="#7BDCB5"/>
                                <stop offset="1" stopColor="#3975D2"/>
                            </linearGradient>
                            <linearGradient id="part_to_do" x1="0" y1="0" x2="500" y2="531"
                                            gradientUnits="userSpaceOnUse">
                                <stop stopColor="#CCCCCC"/>
                                <stop offset="1" stopColor="#CCCCCC"/>
                            </linearGradient>
                        </defs>
                    </svg>
                    <div className="progress__absolute-widget progress__absolute-widget--1">
                        <img src={step >= 1 ? step1 : step1ToDo} alt="Étape 1" onClick={handleClick(1)}/>
                    </div>
                    <p className="progress__absolute-text progress__absolute-text--1">
                        Bienvenue<br/>chez Neo !
                    </p>
                    <div className="progress__absolute-widget progress__absolute-widget--2">
                        <img src={step >= 2 ? step2 : step2ToDo} alt="Étape 2" onClick={handleClick(2)}/>
                    </div>
                    <p className="progress__absolute-text progress__absolute-text--2">
                        On se connait mieux maintenant,<br/>nous avons ton CV !
                    </p>
                    <div className="progress__absolute-widget progress__absolute-widget--3">
                        <img src={step >= 3 ? step3 : step3ToDo} alt="Étape 3" onClick={handleClick(3)}/>
                    </div>
                    <p className="progress__absolute-text progress__absolute-text--3">
                        Tu te lances ?<br/>Postule !
                    </p>
                    <div className="progress__absolute-widget progress__absolute-widget--4">
                        <img src={step >= 4 ? step4 : step4ToDo} alt="Étape 4" onClick={handleClick(4)}/>
                    </div>
                    <p className="progress__absolute-text progress__absolute-text--4">
                        Tu as postulé,<br/>bonne chance !
                    </p>
                </div>
            </div>
        </div>
    );
}

export default ParcoursScreen;

/*  width: 100%;
  display: flex;
  justify-content: center;*/
