import './main.css';
import MainRouter from "../routes/MainRouter";
import TabNavigator from "../component/TabNavigator";
import mainRouterConfig from '../routes/main-router.config'

function MainScreen() {

    return (
        <div className="app-main">
            <nav className="app-main__navigation">
                <TabNavigator config={mainRouterConfig}/>
            </nav>

            <div className="app-main__body">
                <MainRouter config={mainRouterConfig}/>
            </div>
        </div>
    );
}

export default MainScreen;
