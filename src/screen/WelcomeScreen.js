import {
    Link
} from "react-router-dom";
import './welcome.css';
import appLogo from '../assets/logo-neo.svg';
import appTouch from '../assets/app-touch.svg';

function WelcomeScreen() {
    return (
        <div className="app-welcome">
            <header className="app-welcome__header">
                <img className="app-welcome__header__logo" src={appLogo} alt="Logo Neo By Randstad"/>
            </header>
            <nav className="app-welcome__navigation">
                <ul className="navigation navigation--pills">
                    <li className="navigation__widget">
                        <Link className="navigation__link navigation__link--first" to="/connexion">Connexion</Link>
                    </li>
                    <li className="navigation__widget">
                        <Link className="navigation__link" to="/inscription">Inscription</Link>
                    </li>
                </ul>
            </nav>
            <img className="app-welcome__footer" src={appTouch} alt="Interaction application mobile"/>
        </div>
    );
}

export default WelcomeScreen;
