import {useEffect} from "react";

function InscriptionScreen({setUser}) {

    useEffect(() => {setUser({})});

    return (
        <p>Inscription</p>
    );
}

export default InscriptionScreen;
