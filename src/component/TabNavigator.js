import {Link, useLocation} from "react-router-dom";
import './tabNavigator.css';

function TabNavigator({config}) {
    const {pathname} = useLocation();
    return (
        <ul className="navigation navigation--tabs">
            {
                config.map(
                    (route, index) => (
                        <li key={index} className={pathname === route.path ? "navigation__widget navigation__widget--active" : "navigation__widget"}>
                            <Link to={route.path} className="navigation__link">
                                <img src={route.icon} alt={route.name} className="navigation__img"/>
                                <p className="navigation__text">{route.name}</p>
                            </Link>
                        </li>
                    )
                )
            }
        </ul>
    )
}

export default TabNavigator
