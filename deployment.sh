git checkout master
yarn build
rm -rf ./build-prod/*

cp -r ./build/* ./build-prod/
echo '{}' > ./build-prod/composer.json

NOW = $(date +"%d-%m-%Y %H:%M")
cd ./build-prod && git add . && git commit -m "Deployment from $NOW"
git push heroku prod:main
cd ..
